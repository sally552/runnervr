﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolAreas : MonoBehaviour
{
    //ленивая реализация пула
    [SerializeField]
    private List<PoolObject> areas;
    private List<Queue<GameObject>> pool = new List<Queue<GameObject>>();

    private CalculatorAreaConst calculatorPosition = new CalculatorAreaConst();

    private void Start()
    {
        InitPool();
        for (int i = 0; i < 4; i++)
        {
            SpawnObjectPool();
        }
    }

    private void InitPool()
    {
        foreach (PoolObject objectPool in areas)
        {
            //нужны именно с интерфейсом
            if (objectPool.Prefab != null)
            {
                var po = objectPool.Prefab.GetComponent<IPoolObject>();
                if (po != null)
                {
                    var queue = new Queue<GameObject>();
                    for (int i = 0; i < objectPool.CountInPool; i++)
                    {
                        var go = Instantiate(objectPool.Prefab);
                        go.GetComponent<IPoolObject>().OnFinish += () =>
                        {
                            SpawnObjectPool();
                        };
                        go.SetActive(false);
                        queue.Enqueue(go);
                    }
                    pool.Add(queue);
                }
            }
        }
    }

    private int RandomizerAreas()
    {
        return Random.Range(0, pool.Count);
    }

    public void SpawnObjectPool()
    {
        int value = RandomizerAreas();
        var objectSpawn = pool[value].Dequeue();
        objectSpawn.SetActive(true);
        objectSpawn.transform.position = calculatorPosition.GetPosition();

        IPoolObject po = objectSpawn.GetComponent<IPoolObject>();
        po.OnObjectSpawn();

        pool[value].Enqueue(objectSpawn);
        //return objectSpawn;
    }

    [System.Serializable]
    public class PoolObject
    {
        public GameObject Prefab;
        public int CountInPool;
    }
}
