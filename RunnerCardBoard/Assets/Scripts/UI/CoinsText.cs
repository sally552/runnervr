﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsText : MonoBehaviour
{
    private Text text;
    private int coins = 0;

    private void Start()
    {
        text = GetComponent<Text>();
        WritingCoins();
        var player = GameObject.FindObjectOfType<PlayerPickerCoins>();
        player.OnPickCoin += WritingCoins;
    }

    private void WritingCoins()
    {
        text.text = "Coins: " + (coins++).ToString();
    }
}
