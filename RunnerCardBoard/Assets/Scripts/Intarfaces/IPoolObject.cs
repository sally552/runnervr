﻿using System;

public interface IPoolObject
{
    void OnObjectSpawn();
    Action OnFinish { get; set; }
    void SwithedActiveObject();
}
