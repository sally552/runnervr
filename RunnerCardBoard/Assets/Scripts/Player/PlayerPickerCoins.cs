﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerPickerCoins : MonoBehaviour
{
    public Action OnPickCoin;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Coin") && OnPickCoin != null)
        {
            OnPickCoin.Invoke();
        }
    }
}
