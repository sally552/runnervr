﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField]
    private float deathDeep = 7;
    private Transform transformPlayer;

    private void Start()
    {
        transformPlayer = GetComponent<Transform>();
    }

    private void Update()
    {
        if (transformPlayer.position.y < -deathDeep)
        {
            Death();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Death"))
        {
            Death();
        }
    }

    private void Death()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
