﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float speed = 10f;

    [SerializeField]
    private float speedLateral = 0.5f;
    private Rigidbody rigiBody;

    [SerializeField]
    private Transform mainCamera;
    private float directionAngle;

    private void Start()
    {
        rigiBody = GetComponent<Rigidbody>();
    }

    //физика...
    private void FixedUpdate()
    {
        directionAngle = mainCamera.rotation.eulerAngles[2];
        if (directionAngle > 180)
            directionAngle -= 360;

        if (Input.GetKey(KeyCode.A))
            directionAngle = 1;
        if (Input.GetKey(KeyCode.D))
            directionAngle = -1;
        rigiBody.MovePosition(rigiBody.position + Vector3.forward * speed * Time.fixedDeltaTime + 
            Vector3.left * speedLateral * directionAngle * Time.fixedDeltaTime);
    }
}
