﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaController : MonoBehaviour, IPoolObject
{
    public Action OnFinish{ get; set; }

    private void Start()
    {
        OnFinish += SwithedActiveObject;
    }

    //появление объекта == OnEnable
    public void OnObjectSpawn()
    {
    }

    //переключение
    public void SwithedActiveObject()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && OnFinish != null)
        {
            OnFinish.Invoke();
        }

    }
}
