﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    [Header("размер зоны")]
    [SerializeField]
    private float amplitude;

    [SerializeField]
    private float speedCube;

    private float time= 0;
    private float initialAngel;
    private Vector3 initialPos;

    private Transform tr;

    private void Start()
    {
        tr = GetComponent<Transform>();
        speedCube = Random.Range(2, speedCube);
        initialAngel = Random.Range(0, Mathf.PI * 2);
        initialPos = tr.localPosition;
    }

    private void Update()
    {
        time += Time.deltaTime;
        tr.localPosition = initialPos + new Vector3(Mathf.Sin(speedCube * time + initialAngel) * amplitude, 0, 0);
    }
}

