﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Coin : MonoBehaviour
{
    private Transform tr;

    [SerializeField]
    private float rotSpeed = 5;

    private void Start()
    {
        tr = GetComponent<Transform>();
    }

    private void Update()
    {
        tr.rotation = tr.rotation * Quaternion.Euler(0, 0, rotSpeed);
    }
}
